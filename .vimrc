execute pathogen#infect()
syntax on
filetype plugin indent on
autocmd VimEnter * NERDTree
colorscheme monokai_pro

let g:lightline = {
      \ 'colorscheme': 'wombat',
      \ }

set mouse=a
set autoread
au CursorHold * checktime

set tabstop=1
set expandtab 
set shiftwidth=1

set incsearch 

set noshowmode

set hlsearch
set number
set numberwidth=2
let g:default_julia_version = "devel"
let g:NERDTreeWinPos = "right"
